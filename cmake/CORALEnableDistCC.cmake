 # Add to a project the possibility of using distcc during compilation.
 # Note: it must be included after "CORALEnableCCache.cmake"
find_program(distcc_cmd distcc)
mark_as_advanced(distcc_cmd)

if(distcc_cmd)
  option(CMAKE_USE_DISTCC "Use distcc to speed up compilation." OFF)
  if(CMAKE_USE_DISTCC)
    if(ccache_cmd AND CMAKE_USE_CCACHE)
      set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "CCACHE_PREFIX=${distcc_cmd} ${ccache_cmd}")
      message(STATUS "Enabling distcc builds in ccache")
    else()
      set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ${distcc_cmd})
      message(STATUS "Using distcc for building")
    endif()
  endif()
endif()
