# - Find QMTest.
#
# This module will define the following variables:
#  QMTEST_EXECUTABLE  - the qmtest main script
#  QMTEST_PYTHON_PATH - directory containing the Python module 'qm'

find_program(QMTEST_EXECUTABLE
  NAMES qmtest
  PATHS ${QMtest_home}/bin
        ${QMtest_home}/Scripts
  HINTS ${QMTEST_ROOT_DIR}/bin
        $ENV{QMTEST_ROOT_DIR}/bin
)

if (QMTEST_EXECUTABLE)
  get_filename_component(QMTEST_BINARY_PATH ${QMTEST_EXECUTABLE} PATH)
  get_filename_component(QMTEST_PREFIX_PATH ${QMTEST_BINARY_PATH} PATH)
  # Define Python_config_version_twodigit if not yet defined
  if(NOT Python_config_version_twodigit)
    execute_process(COMMAND ${PYTHON_EXECUTABLE} -V ERROR_VARIABLE Python_config_version_twodigit)
    string(REGEX MATCH "[0-9]+\\.[0-9]+" Python_config_version_twodigit ${Python_config_version_twodigit})
  endif()
  # Find QMTEST_PYTHON_PATH below QMTEST_PREFIX_PATH if defined
  find_path(QMTEST_PYTHON_PATH
    NAMES qm/__init__.py
    PATHS ${QMTEST_PREFIX_PATH}/lib/python${Python_config_version_twodigit}/site-packages ${QMTEST_PREFIX_PATH}/Lib/site-packages)
endif()

if (NOT QMTEST_PYTHON_PATH)
  # let's try to find the qm module in the standard environment
  # NB: On Python3 LCG stacks, QMTest in CORAL/COOL uses internally Python2,
  # NB: i.e. the "python" executable, from the O/S (CORALCOOL-2954)
  if (LCG_python3 STREQUAL on) # CORALCOOL-2976
    message(STATUS "Looking for python3")
    find_package(PythonInterp 3)
  else()
    message(STATUS "Looking for python2")
    find_package(PythonInterp)
  endif()
  execute_process(COMMAND ${PYTHON_EXECUTABLE} -c "import os, qm; print os.path.dirname(os.path.dirname(qm.__file__))"
                  OUTPUT_VARIABLE QMTEST_PYTHON_PATH OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
  if (QMTEST_PYTHON_PATH)
    # we found it, so no need to extend the PYTHONPATH
    set(QMTEST_PYTHON_PATH ${QMTEST_PYTHON_PATH} CACHE "" "" FORCE)
  endif()
endif()

# handle the QUIETLY and REQUIRED arguments and set QMTEST_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(QMTest DEFAULT_MSG QMTEST_EXECUTABLE QMTEST_PYTHON_PATH)

mark_as_advanced(QMTEST_EXECUTABLE QMTEST_PYTHON_PATH)

# Remove QM_home from the environment, it is no longer needed (CORALCOOL-2858)
###if (QMTEST_PREFIX_PATH) # FIX relevant to CORALCOOL-2793
###  set(QMTEST_ENVIRONMENT SET QM_home ${QMTEST_PREFIX_PATH})
###  if(WIN32)
###    set(QMTEST_LIBRARY_PATH ${QMTEST_PREFIX_PATH}/Lib/site-packages/pywin32_system32)
###  endif()
###endif()

