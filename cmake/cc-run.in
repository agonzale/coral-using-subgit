#!/bin/bash
if [ "$BASH_SOURCE" != "$0" ]; then # See CORALCOOL-2823
  echo "ERROR! This script was sourced?" > /dev/stderr
  return 1
fi
topdirl=`dirname $0`
topdirl=`cd $topdirl; pwd -L`

# Check optional arguments
msg=0
if [ "$1" == "-q" ]; then
  msg=q
  shift
elif [ "$1" == "-v" ]; then
  msg=v
  shift
fi

# Check mandatory arguments
if [ "$1" == "" ]; then
  echo "Usage: $0 [-q|-v] command [arguments]"
  exit 1
fi

# Read original CORALSYS/COOLSYS and LCG_releases_base from project xenv
# [NB these values are always REALPATHs! see CORALCOOL-2876 and CORALCOOL-2836]
envxml=$topdirl/env/@env_xml_basename@
project=`basename $envxml .xenv`
projsysold=`more $envxml | grep variable=\"${project}SYS\" | tr "<" " " | tr ">" " " | awk '{print $3}'`
lcgrelold=`more $envxml | grep variable=\"LCG_releases_base\" | tr "<" " " | tr ">" " " | awk '{print $3}'`
if [ "$msg" == "v" ]; then 
  echo ""
  echo "[DEBUG] Read build-time values from $envxml"
  echo "[DEBUG] old -> ${project}SYS = $projsysold"
  echo "[DEBUG] old -> LCG_releases_base = $lcgrelold"
fi

# Do original CORALSYS/COOLSYS and LCG_releases_base respect lcgcmake policies?
# [NB this check is not always needed, but we do it here for better readability]
# [NB the check makes sense as both are REALPATHs, see above and CORALCOOL-2876]
lcgrelold_lcgcmake=`dirname $projsysold`
lcgrelold_lcgcmake=`dirname $lcgrelold_lcgcmake`
lcgrelold_lcgcmake=`dirname $lcgrelold_lcgcmake`
if [ "$lcgrelold" == "$lcgrelold_lcgcmake" ]; then
  if [ "$msg" == "v" ]; then 
    echo "[DEBUG] Build-time values are consistent with lcgcmake paths"
  fi
  lcgcmake_paths=1
else
  if [ "$msg" == "v" ]; then 
    echo "[DEBUG] Build-time values are NOT consistent with lcgcmake paths"
  fi
  lcgcmake_paths=0
fi

# Read original pythonExecutable from this file cc-run
# [NB pythonExecutable from lcgcmake contains the package version without hash]
# [NB lcgversion is not always needed, but we do it here for better readability]
pythonExecutable=@PYTHON_EXECUTABLE_CCPATH@ # may start by {LCG_releases_base}
lcgversion=@LCG_VERSION@
if [ "$msg" == "v" ]; then 
  echo ""
  echo "[DEBUG] Read build-time values from cc-run"
  echo "[DEBUG] old -> python = $pythonExecutable"
  echo "[DEBUG] old -> LCG_VERSION = $lcgversion"
  echo ""
fi

# Check if LCG_releases_base has been set before calling this script
# Determine the path to the python executable accordingly
if [ "$LCG_releases_base" != "" ]; then
  pythonExecutable=`echo ${pythonExecutable} | sed "s|^{LCG_releases_base}|${LCG_releases_base}|"`
  if [ "$msg" == "v" ]; then 
    echo "[DEBUG] LCG_releases_base found in environment: OVERRIDE it"
    echo "[DEBUG] new -> LCG_releases_base = $LCG_releases_base"
    echo "[DEBUG] new -> python = $pythonExecutable"
    echo ""
  fi
else
  pythonExecutable=`echo ${pythonExecutable} | sed "s|^{LCG_releases_base}|${lcgrelold}|"`
  if [ "$msg" == "v" ]; then 
    echo "[DEBUG] LCG_releases_base NOT found in environment: use xenv value"
    echo "[DEBUG] new -> LCG_releases_base = $lcgrelold"
    echo "[DEBUG] new -> python = $pythonExecutable"
    echo ""
  fi
fi

# Is this directory in the original location determined by 'make install' 
# or was this relocated elsewhere (eg via lcgcmake rpm installation)? 
projsysnew=$topdirl
if [ -d $projsysold ]; then projsysoldp=`cd $projsysold; pwd -P`; fi
if [ -d $projsysnew ]; then projsysnewp=`cd $projsysnew; pwd -P`; fi
if [ "$msg" == "v" ]; then 
  echo "[DEBUG] Is this directory in its original build-time location?"
  echo "[DEBUG] old -> ${project}SYS = $projsysold"
  echo "[DEBUG] new -> ${project}SYS = $projsysnew"
  echo "[DEBUG] Resolve all symlinks before comparing original and current locations"
  echo "[DEBUG] old (phys) -> ${project}SYS = $projsysoldp"
  echo "[DEBUG] new (phys) -> ${project}SYS = $projsysnewp"
  echo ""
fi

# Override CORALSYS/COOLSYS if relocated (CORALCOOL-2829, CORALCOOL-2876)
if [ "$projsysnewp" == "$projsysoldp" ]; then
  if [ "$msg" == "v" ]; then 
    echo "[DEBUG] This directory is in its original build-time location"
    echo "[DEBUG] new -> ${project}SYS = $projsysold"
    echo ""
  fi
else
  if [ "$project" == "CORAL" ]; then
    export CORALSYS=$projsysnew
  else
    export COOLSYS=$projsysnew
  fi
  if [ "$msg" == "v" ]; then 
    echo "[DEBUG] This directory was moved: OVERRIDE ${project}SYS"
    echo "[DEBUG] new -> ${project}SYS = $projsysnew"
    echo ""
  fi

  # If LCG_releases_base has been set before calling this script,
  # use the externally provided value: nothing else to do here
  if [ "$LCG_releases_base" != "" ]; then
    if [ "$msg" == "v" ]; then 
      echo "[DEBUG] LCG_releases_base found in environment: keep it"
      echo "[DEBUG] new -> LCG_releases_base = $LCG_releases_base"
      echo "[DEBUG] new -> python = $pythonExecutable"
      echo ""
    fi

  # Override LCG_releases_base (including python and all other externals)
  # if installation was relocated AND it follows lcgcmake path policies
  else
    if [ "$msg" == "v" ]; then 
      echo "[DEBUG] LCG_releases_base NOT found in environment"
    fi
    if [ "$lcgcmake_paths" == "0" ]; then
      if [ "$msg" == "v" ]; then 
        # Example: relocated local CORAL build against fixed LCG_release_base
        echo "[DEBUG] Build-time LCG_releases_base NOT consistent with lcgcmake: keep as-is"
        echo "[DEBUG] new -> LCG_releases_base = $lcgrelold"
        echo "[DEBUG] new -> python = $pythonExecutable"
        echo ""
      fi
    else
      lcgrelnew=`dirname $projsysnew` 
      lcgrelnew=`dirname $lcgrelnew`
      lcgrelnew=`dirname $lcgrelnew`
      export LCG_releases_base=$lcgrelnew
      pythonExecutable=`echo ${pythonExecutable} | sed "s|^${lcgrelold}|${lcgrelnew}|"`
      if [ "$msg" == "v" ]; then 
        echo "[DEBUG] Build-time LCG_releases_base consistent with lcgcmake: OVERRIDE it"
        echo "[DEBUG] new -> LCG_releases_base = $LCG_releases_base"
        echo "[DEBUG] new -> python = $pythonExecutable"
        echo ""
      fi
      # If pythonExecutable is not found, append LCG_VERSION (CORALCOOL-2856)
      if [ ! -f ${pythonExecutable} ] && [ "$lcgversion" != "" ]; then
        lcgrelnew2=$lcgrelnew/LCG_$lcgversion
        export LCG_releases_base=$lcgrelnew2
        pythonExecutable=`echo ${pythonExecutable} | sed "s|^${lcgrelnew}|${lcgrelnew2}|"`
        if [ "$msg" == "v" ]; then 
          echo "[DEBUG] Python not found: OVERRIDE it by appending LCG_VERSION"
          echo "[DEBUG] new -> LCG_releases_base = $LCG_releases_base"
          echo "[DEBUG] new -> python = $pythonExecutable"
          echo ""
        fi
      fi
    fi
  fi
fi

# Protections against pre-existing PYTHONHOME and PYTHONPATH (CORALCOOL-2803)
unset PYTHONHOME
unset PYTHONPATH

# Set the path to the python library on Darwin (CORALCOOL-2883)
sys=`${pythonExecutable} -c 'import platform; print ( platform.system() )'` 

# Run the specified command in the correct runtime environment
# [Make sure the minimum PATH is defined in case this is run through 'env -i']
if [ "$sys" == "Darwin" ]; then
  PATH=$PATH:/usr/local/bin:/bin:/usr/bin DYLD_LIBRARY_PATH=`dirname ${pythonExecutable}`/../lib ${pythonExecutable} $topdirl/env/xenv --xml=$envxml "$@"
else
  PATH=$PATH:/usr/local/bin:/bin:/usr/bin ${pythonExecutable} $topdirl/env/xenv --xml=$envxml "$@"
fi
status=$?
if [ "$msg" != "q" ] && [ "$status" != "0" ]; then 
  echo "ERROR! Failed to execute command \"$*\""
  echo PYTHON=${pythonExecutable}
  exit $status
fi
exit $status
