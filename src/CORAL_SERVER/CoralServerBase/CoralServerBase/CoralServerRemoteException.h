#ifndef CORALSERVERBASE_CORALSERVERREMOTEEXCEPTION_H
#define CORALSERVERBASE_CORALSERVERREMOTEEXCEPTION_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral
{

  /** @class CoralServerRemoteException
   *
   *  Exception thrown by CORAL_SERVER clients when a packet encoding 
   *  a server-side remote exception is received. See CORALCOOL-1522.
   *
   *  @author Andrea Valassi
   *  @date   2016-08-19
   */

  class CoralServerRemoteException : public CoralServerBaseException
  {

  public:

    /// Constructor
    CoralServerRemoteException( const std::string& message,
                                const std::string& method,
                                const std::string& module )
      : CoralServerBaseException( message, method, module ){}

    /// Destructor
    virtual ~CoralServerRemoteException() throw() {}

  };

}
#endif // CORALSERVERBASE_CORALSERVERREMOTEEXCEPTION_H
