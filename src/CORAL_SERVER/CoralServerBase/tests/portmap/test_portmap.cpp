#include "CoralBase/../tests/Common/CoralCppUnitTest.h"

// Include files
#include "CoralServerBase/portmap.h"

// Namespace
using namespace coral;

namespace coral {

class PortParserTest: public CoralCppUnitTest {

    CPPUNIT_TEST_SUITE (PortParserTest);
    CPPUNIT_TEST (test_parseSimplePort);
    CPPUNIT_TEST (test_parseRpcPort);
    CPPUNIT_TEST (test_parseRpcPortHash);
    CPPUNIT_TEST (test_parseFail);
    CPPUNIT_TEST_SUITE_END();

public:

    void setUp() {
    }

    void tearDown() {
    }

    // ------------------------------------------------------

    void test_parseSimplePort() {
        std::pair<uint32_t, uint32_t> port;

        port = parsePort("1");
        CPPUNIT_ASSERT_EQUAL(0U, port.first);
        CPPUNIT_ASSERT_EQUAL(1U, port.second);

        port = parsePort("100");
        CPPUNIT_ASSERT_EQUAL(0U, port.first);
        CPPUNIT_ASSERT_EQUAL(100U, port.second);

        port = parsePort("65535");
        CPPUNIT_ASSERT_EQUAL(0U, port.first);
        CPPUNIT_ASSERT_EQUAL(65535U, port.second);

        port = parsePort("0xff");
        CPPUNIT_ASSERT_EQUAL(0U, port.first);
        CPPUNIT_ASSERT_EQUAL(255U, port.second);

        port = parsePort("0xffff");
        CPPUNIT_ASSERT_EQUAL(0U, port.first);
        CPPUNIT_ASSERT_EQUAL(65535U, port.second);
    }

    // ------------------------------------------------------

    void test_parseRpcPort() {
        std::pair<uint32_t, uint32_t> port;

        port = parsePort("[1]");
        CPPUNIT_ASSERT_EQUAL(1U, port.first);
        CPPUNIT_ASSERT_EQUAL(1U, port.second);

        port = parsePort("[255]");
        CPPUNIT_ASSERT_EQUAL(255U, port.first);
        CPPUNIT_ASSERT_EQUAL(1U, port.second);

        port = parsePort("[0xFF]");
        CPPUNIT_ASSERT_EQUAL(255U, port.first);
        CPPUNIT_ASSERT_EQUAL(1U, port.second);

        port = parsePort("[1.999]");
        CPPUNIT_ASSERT_EQUAL(1U, port.first);
        CPPUNIT_ASSERT_EQUAL(999U, port.second);

        port = parsePort("[1.0x666]");
        CPPUNIT_ASSERT_EQUAL(1U, port.first);
        CPPUNIT_ASSERT_EQUAL(0x666U, port.second);
    }

        // ------------------------------------------------------

    void test_parseRpcPortHash() {
        // this assumes CRC32 used for hashing

        std::pair<uint32_t, uint32_t> port;

        port = parsePort("[1.#]");
        CPPUNIT_ASSERT_EQUAL(1U, port.first);
        CPPUNIT_ASSERT_EQUAL(0U, port.second);

        port = parsePort("[1.#X]");
        CPPUNIT_ASSERT_EQUAL(1U, port.first);
        CPPUNIT_ASSERT_EQUAL(3081909835U, port.second);

        port = parsePort("[1.##HASH#]");
        CPPUNIT_ASSERT_EQUAL(1U, port.first);
        CPPUNIT_ASSERT_EQUAL(2414665341U, port.second);

        port = parsePort("[0x123.#ABC[123]]");
        CPPUNIT_ASSERT_EQUAL(0x123U, port.first);
        CPPUNIT_ASSERT_EQUAL(457644305U, port.second);
    }

    // ------------------------------------------------------

    void test_fail1(const char* str) {
        std::pair<uint32_t, uint32_t> port;

        port = parsePort(str);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(str, 0U, port.first);
        CPPUNIT_ASSERT_EQUAL_MESSAGE(str, 0U, port.second);
    }

    void test_parseFail() {
        test_fail1("");
        test_fail1("a");
        test_fail1("123a");
        test_fail1("[1");
        test_fail1("1]");
        test_fail1("[1a]");
        test_fail1("[1.]");
        test_fail1("[1.xxx]");
        test_fail1("[.255]");
        // out-of range test
        test_fail1("65536");
        test_fail1("1000000");
        test_fail1("0x10000");
    }

    // ------------------------------------------------------

};

CPPUNIT_TEST_SUITE_REGISTRATION (PortParserTest);

}

CORALCPPUNITTEST_MAIN( PortParserTest )
