#ifndef CORALSERVERPROXY_MSGLOGGER_H
#define CORALSERVERPROXY_MSGLOGGER_H

//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id: MsgLogger.h,v 1.1.2.3 2010-12-20 11:27:02 avalassi Exp $
//
// Description:
//	Class MsgLogger.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoralBase/MessageStream.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

// User-friendly macros [yuck]
#define PXY_MSG(LVL,MSG) \
    do { if (coral::LVL >= coral::MessageStream::msgVerbosity()) { \
        coral::MessageStream("proxy") << coral::LVL << MSG << coral::MessageStream::endmsg; }\
    } while (false)
#define PXY_ERR(msg) PXY_MSG(Error,msg)
#define PXY_WARN(msg) PXY_MSG(Warning,msg)
#define PXY_INFO(msg) PXY_MSG(Info,msg)
#define PXY_TRACE(msg) PXY_MSG(Debug,msg)
#define PXY_DEBUG(msg) PXY_MSG(Verbose,msg)

#endif // CORALSERVERPROXY_MSGLOGGER_H
