find_package(Boost REQUIRED thread)

include(CORALSharedLib)
CORALSharedLib(LIBS lcg_CoralServerBase ${Boost_LIBRARIES}
               UTILITIES coralDummyPollServer
                         coralDummySocketClient
                         coralDummySocketServer
                         coralReplayClient
                         coralServerStressClient
               TESTS DummyRequestHandler
                     PacketSocket
                     Poll
                     ReplyManager
                     RequestIterator
                     RingBufferSocket
                     SocketContext
                     SocketRequestHandler
                     SocketThread
                     SslSocket
                     TcpSocket
                     ThreadManager
               NO_HEADERS)

# Special link options
find_package(Boost REQUIRED filesystem)
target_link_libraries(coralReplayClient ${Boost_LIBRARIES})
