#ifndef CORALBASE_CORALTHREADGROUPHEADERS_H
#define CORALBASE_CORALTHREADGROUPHEADERS_H 1

#include "CoralBase/../src/coral_mutex_headers.h" // for CORAL_HAS_CPP11_THREADS

// Include files
#ifdef CORAL_HAS_CPP11_THREADS
#include <algorithm> // for coral::thread_group implementation
#include <list>      // for coral::thread_group implementation
#endif

// Typedef coral::thread_group for CORAL internals (task #50199)
namespace coral
{

#ifndef CORAL_HAS_CPP11_THREADS

  typedef boost::thread_group              thread_group;

#else

  //std::thread_group is missing in c++11: implement coral::thread_group below

  class thread_group
  {

  protected:

    typedef std::list<std::thread*> thread_list_t;

    thread_list_t thread_list;

  public:

    thread_group( const thread_group& ) = delete;

    thread_group& operator = ( const thread_group& ) = delete;

    thread_group()
    {
    }

    ~thread_group()
    {
      for ( auto thr : thread_list ) { delete thr; }
    }

    bool is_thread_in( const std::thread* thr ) const
    {
      auto position = std::find( thread_list.begin(), thread_list.end(), thr );
      return position != thread_list.end();
    }

    bool is_this_thread_in() const
    {
      auto this_thread_id = std::this_thread::get_id();
      for ( auto thread : thread_list )
      {
        if ( thread->get_id() == this_thread_id ) return true;
      }
      return false;
    }

    void add_thread( std::thread* thr )
    {
      if ( !is_thread_in( thr ) ) thread_list.push_back( thr );
    }

    template <typename F> void create_thread( F& thrfunc )
    {
      add_thread( new std::thread( thrfunc ) );
    }

    void remove_thread( std::thread* thr )
    {
      auto position = std::find( thread_list.begin(), thread_list.end(), thr );
      if ( position != thread_list.end() )
      {
        delete *position;
        thread_list.erase( position );
      }
    }

    void join_all()
    {
      if ( is_this_thread_in() ) return;
      for ( auto thread : thread_list ) { thread->join(); }
    }

  };

#endif
}

#endif // CORALBASE_CORALTHREADGROUPHEADERS_H
