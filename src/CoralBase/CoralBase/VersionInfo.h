#ifndef CORALBASE_VERSIONINFO_H
#define CORALBASE_VERSIONINFO_H 1

// This switch is no longer needed in trunk (CORALCOOL-1111, CORALCOOL-2943)
//#define CORAL300 1 // CORAL 3.0 or higher (CORAL 2.x is no longer supported!)

// This switch is no longer needed in trunk (CORALCOOL-1111, CORALCOOL-2943)
//#define CORAL240 1 // CORAL 2.4 or higher (CORAL 2.3 is no longer supported!)

// This switch is no longer needed in trunk (CORALCOOL-2808, CORALCOOL-2809)
//#define CORAL_HAS_CPP11 1 // CORAL code uses c++11 

// CORAL_VERSIONINFO_RELEASE[_x] is #defined as of CORAL 2.3.13 (CORALCOOL-1504)
#define CORAL_VERSIONINFO_RELEASE_MAJOR 3
#define CORAL_VERSIONINFO_RELEASE_MINOR 1
#define CORAL_VERSIONINFO_RELEASE_PATCH 8
#define CORAL_VERSIONINFO_RELEASE CORAL_VERSIONINFO_RELEASE_MAJOR.CORAL_VERSIONINFO_RELEASE_MINOR.CORAL_VERSIONINFO_RELEASE_PATCH

// Sanity check: does this compiler support c++11? (see CORALCOOL-2808)
#if ( ! defined(__GXX_EXPERIMENTAL_CXX0X__) ) && (__cplusplus < 201103L )
#error("ERROR: CORAL >= 3.0.0 but this compiler does not support c++11")
#endif

#endif // CORALBASE_VERSIONINFO_H
