#ifndef CORALBASE_CORALFILESYSTEMHEADERS_H
#define CORALBASE_CORALFILESYSTEMHEADERS_H 1

// NB: coral_filesystem_headers should be _completely_ removed in all branches!

// FIXME! Should use c++11 instead of boost in the internal implementation!
// NB: boost_filesystem_headers should be _completely_ removed in CORAL3 branch!
#include "CoralBase/../src/boost_filesystem_headers.h" // SHOULD BE REMOVED!

#endif // CORALBASE_CORALFILESYSTEMHEADERS_H
