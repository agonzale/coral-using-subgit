#include "CoralBase/MessageStream.h"
#include "CoralBase/../tests/Common/CoralCppUnitDBTest.h"

#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/ICursor.h" // Needed on WIN32
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITransaction.h"

#include "CoralKernel/Service.h"
#include "CoralKernel/Context.h"

#include <iostream>

// The connection strings for CORAL tests
const std::string UrlRW = coral::CoralCppUnitDBTest::BuildUrl( "Oracle", false );
const std::string UrlRO = coral::CoralCppUnitDBTest::BuildUrl( "Frontier", true );

int main( int, char** )
{

  try
  {
    coral::ConnectionService connSvc;

    // 1. Write sample data
    std::cout << "_TEST Write sample data" << std::endl;
    coral::AccessMode accessModeW = coral::Update;
    std::auto_ptr<coral::ISessionProxy>
      sessionW( connSvc.connect( UrlRW, accessModeW ) );
    sessionW->transaction().start( false );
    sessionW->transaction().commit();

    // 2. Read sample data
    std::cout << "_TEST Read sample data" << std::endl;
    coral::AccessMode accessModeR = coral::ReadOnly;
    std::auto_ptr<coral::ISessionProxy>
      sessionR( connSvc.connect( UrlRO, accessModeR ) );
    sessionR->transaction().start( true );
    std::auto_ptr<coral::IQuery> query( sessionR->nominalSchema().newQuery() );
    query->addToTableList( "DUAL" );
    query->addToOutputList( "1" );
    std::cout << "_TEST Execute the query" << std::endl;
    coral::MsgLevel lvl = coral::MessageStream::msgVerbosity();
    coral::MessageStream::setMsgVerbosity( coral::Debug );
    // This was failing with a segfault (bug #19758 aka CORALCOOL-439)
    // It is now failing to select from DUAL in Frontier (CORALCOOL-876)
    try
    {
      query->execute();
    }
    catch(...)
    {
      coral::MessageStream::setMsgVerbosity( lvl );
      throw;
    }
    coral::MessageStream::setMsgVerbosity( lvl );
    std::cout << "_TEST Executed the query - now commit" << std::endl;
    sessionR->transaction().commit();
    std::cout << "_TEST Completed successfully" << std::endl;

  }

  catch ( std::exception& e ) {
    std::cerr << "Standard C++ exception caught: " << e.what() << std::endl;
    return 1;
  }

  catch ( ... ) {
    std::cerr << "Unknown exception caught" << std::endl;
    return 1;
  }

  return 0;

}
